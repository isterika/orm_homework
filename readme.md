# Домашнее задание ORM Bitrix

*Maxim G*

## Описание сущностей
* [modules/glushkov.orm/lib/article.php](https://bitbucket.org/isterika/orm_homework/src/master/modules/glushkov.orm/lib/article.php) - описание сущности Статья
* [modules/glushkov.orm/lib/author.php](https://bitbucket.org/isterika/orm_homework/src/master/modules/glushkov.orm/lib/author.php) - описание сущности Автор

## Примеры работы
* [modules/glushkov.orm/lib/blogcrud.php](https://bitbucket.org/isterika/orm_homework/src/master/modules/glushkov.orm/lib/blogcrud.php) - работа с объектами статей (создание, чтение, обновление, удаление)
* [modules/glushkov.orm/lib/blogcollection.php](https://bitbucket.org/isterika/orm_homework/src/master/modules/glushkov.orm/lib/blogcollection.php) - работа с коллекциями объектов статей
* [modules/glushkov.orm/lib/bloglinks.php](https://bitbucket.org/isterika/orm_homework/src/master/modules/glushkov.orm/lib/bloglinks.php) - пример работы с отношениями ManyToMany

## Файл аннотаций
* [modules/orm_annotations.php](https://bitbucket.org/isterika/orm_homework/src/master/modules/orm_annotations.php)

## Таблицы
* Таблица articles: http://joxi.ru/n2YVG07cbW0y0A
* Таблица authors: http://joxi.ru/DmB06ReCJK6WxA
* Таблица articles_authors: http://joxi.ru/82QNy43iwJ4pZA
<?php

namespace glushkov\orm\author\AdminInterface;

use DigitalWand\AdminHelper\Helper\AdminEditHelper;

/**
 * Хелпер описывает интерфейс, выводящий форму редактирования новости.
 *
 * {@inheritdoc}
 */
class AuthorEditHelper extends AdminEditHelper
{
	protected static $model = '\glushkov\orm\author\AuthorTable';
}
<?php

namespace glushkov\orm\author\AdminInterface;

use Bitrix\Main\Localization\Loc;
use DigitalWand\AdminHelper\Helper\AdminInterface;
use DigitalWand\AdminHelper\Widget\DateTimeWidget;
use DigitalWand\AdminHelper\Widget\FileWidget;
use DigitalWand\AdminHelper\Widget\NumberWidget;
use DigitalWand\AdminHelper\Widget\StringWidget;
use DigitalWand\AdminHelper\Widget\UrlWidget;
use DigitalWand\AdminHelper\Widget\UserWidget;
use DigitalWand\AdminHelper\Widget\VisualEditorWidget;

Loc::loadMessages(__FILE__);

/**
 * Описание интерфейса (табок и полей) админки новостей.
 *
 * {@inheritdoc}
 */
class AuthorAdminInterface extends AdminInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function fields()
	{
		return array(
			'MAIN' => array(
				'NAME' => 'Автор',

				'FIELDS' => array(
					'ID' => array(
						'WIDGET' => new NumberWidget(),
						'READONLY' => true,
						'FILTER' => true,
					),
					'NAME' => array(
						'WIDGET' => new StringWidget(),
						'SIZE' => '100',
						'FILTER' => '%',
						'REQUIRED' => true
					),

					'DATE_OF_BIRTH' => array(
						'WIDGET' => new DateTimeWidget(),
					),

					'CREATED' => array(
						'WIDGET' => new DateTimeWidget(),
						'READONLY' => true,
					),
					'MODIFIED' => array(
						'WIDGET' => new DateTimeWidget(),
						'READONLY' => true,
					),
				)
			)
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function helpers()
	{
		return array(
			'\glushkov\orm\author\AdminInterface\AuthorListHelper',
			'\glushkov\orm\author\AdminInterface\AuthorEditHelper'
		);
	}
}
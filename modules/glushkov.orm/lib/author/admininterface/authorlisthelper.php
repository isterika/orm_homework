<?php

namespace glushkov\orm\author\AdminInterface;

use DigitalWand\AdminHelper\Helper\AdminListHelper;

/**
 * Хелпер описывает интерфейс, выводящий список новостей.
 *
 * {@inheritdoc}
 */
class AuthorListHelper extends AdminListHelper
{
	protected static $model = '\glushkov\orm\author\AuthorTable';
}
<?php

namespace glushkov\orm\author;

use Bitrix\Main\Entity;
use Bitrix\Main\Type;
use Bitrix\Main\ORM\Fields\Relations\ManyToMany;

class AuthorTable extends Entity\DataManager
{
	public static function getTableName()
	{
		return 'authors';
	}

	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', array(
				'primary'      => true,
				'autocomplete' => true,
			)),
			new Entity\StringField('NAME', [
				'required' => true,
			]),
			new Entity\DateField('DATE_OF_BIRTH'),
			new Entity\DateField('CREATED', [
				'default_value' => new Type\Date
			]),
			new Entity\DateField('MODIFIED', [
				'default_value' => new Type\Date
			]),
			(new ManyToMany('ARTICLE', ArticleTable::class))
				->configureTableName('articles_authors'),
		);
	}
}
<?php

namespace glushkov\orm\article;

use Bitrix\Main\Entity;
use Bitrix\Main\Type;
use Bitrix\Main\ORM\Fields\Relations\ManyToMany;

class ArticleTable extends Entity\DataManager
{
	public static function getTableName()
	{
		return 'articles';
	}

	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', array(
				'primary'      => true,
				'autocomplete' => true,
			)),
			new Entity\StringField('TITLE', [
				'required' => true,
			]),
			new Entity\TextField('PREVIEW_TEXT'),
			new Entity\TextField('DETAIL_TEXT'),
			new Entity\DateField('CREATED', [
				'default_value' => new Type\Date
			]),
			new Entity\DateField('MODIFIED', [
				'default_value' => new Type\Date
			]),
			(new ManyToMany('AUTHOR', AuthorTable::class))
				->configureTableName('articles_authors'),
		);
	}
}
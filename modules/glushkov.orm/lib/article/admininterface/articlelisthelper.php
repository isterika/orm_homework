<?php

namespace glushkov\orm\article\AdminInterface;

use DigitalWand\AdminHelper\Helper\AdminListHelper;

/**
 * Хелпер описывает интерфейс, выводящий список новостей.
 *
 * {@inheritdoc}
 */
class ArticleListHelper extends AdminListHelper
{
	protected static $model = '\glushkov\orm\article\ArticleTable';
}
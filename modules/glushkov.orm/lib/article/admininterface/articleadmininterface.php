<?php

namespace glushkov\orm\article\AdminInterface;

use Bitrix\Main\Localization\Loc;
use DigitalWand\AdminHelper\Helper\AdminInterface;
use DigitalWand\AdminHelper\Widget\DateTimeWidget;
use DigitalWand\AdminHelper\Widget\FileWidget;
use DigitalWand\AdminHelper\Widget\NumberWidget;
use DigitalWand\AdminHelper\Widget\StringWidget;
use DigitalWand\AdminHelper\Widget\UrlWidget;
use DigitalWand\AdminHelper\Widget\UserWidget;
use DigitalWand\AdminHelper\Widget\VisualEditorWidget;

Loc::loadMessages(__FILE__);

/**
 * Описание интерфейса (табок и полей) админки новостей.
 *
 * {@inheritdoc}
 */
class ArticleAdminInterface extends AdminInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function fields()
	{
		return array(
			'MAIN' => array(
				'NAME' => 'Статья',

				'FIELDS' => array(
					'ID' => array(
						'WIDGET' => new NumberWidget(),
						'READONLY' => true,
						'FILTER' => true,
					),
					'TITLE' => array(
						'WIDGET' => new StringWidget(),
						'SIZE' => '100',
						'FILTER' => '%',
						'REQUIRED' => true
					),

					'PREVIEW_TEXT' => array(
						'WIDGET' => new VisualEditorWidget(),
						'HEADER' => false
					),
					'DETAIL_TEXT' => array(
						'WIDGET' => new VisualEditorWidget(),
						'HEADER' => false
					),

					'CREATED' => array(
						'WIDGET' => new DateTimeWidget(),
						'READONLY' => true,
					),
					'MODIFIED' => array(
						'WIDGET' => new DateTimeWidget(),
						'READONLY' => true,
					),
				)
			)
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function helpers()
	{
		return array(
			'\glushkov\orm\article\AdminInterface\ArticleListHelper',
			'\glushkov\orm\article\AdminInterface\ArticleEditHelper'
		);
	}
}
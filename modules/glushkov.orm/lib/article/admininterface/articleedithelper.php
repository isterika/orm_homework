<?php

namespace glushkov\orm\article\AdminInterface;

use DigitalWand\AdminHelper\Helper\AdminEditHelper;

/**
 * Хелпер описывает интерфейс, выводящий форму редактирования новости.
 *
 * {@inheritdoc}
 */
class ArticleEditHelper extends AdminEditHelper
{
	protected static $model = '\glushkov\orm\article\ArticleTable';
}
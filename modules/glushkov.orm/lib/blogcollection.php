<?php

namespace glushkov\orm;

use \glushkov\orm\ArticleTable;
use \glushkov\orm\AuthorTable;

class BlogCollection
{
	static function getAllArticles(int $limit, int $offset = 0)
	{
		return ArticleTable::getList([
			'limit'  => $limit,
			'offset' => $offset,
		])->fetchCollection()->getAll();
	}
}
<?php

namespace glushkov\orm;

use \glushkov\orm\ArticleTable;
use \glushkov\orm\AuthorTable;

class BlogCrud
{
	// create
	static function makeArticle(array $data)
	{
		$article = ArticleTable::createObject();

		$article->setTitle($data['TITLE']);
		$article->setPreviewText($data['PREVIEW_TEXT']);
		$article->setDetailText($data['DETAIL_TEXT']);

		$article->save();

		return $article;
	}

	// read
	static function getArticleById(int $id)
	{
		return ArticleTable::getByPrimary($id)->fetchObject()->collectValues();
	}

	static function getArticleTitleById(int $id)
	{
		return ArticleTable::getByPrimary($id)->fetchObject()->getTitle();
	}

	// update
	static function setArticle(int $id, array $data)
	{
		$article = ArticleTable::getByPrimary($id)->fetchObject();

		foreach ($data as $key => $value) {
			$article->set($key, $value);
		}

		$article->save();

		return $article;
	}

	// delete
	static function removeArticle(int $id)
	{
		$article = ArticleTable::getByPrimary($id)->fetchObject();
		return $article->delete();
	}
}
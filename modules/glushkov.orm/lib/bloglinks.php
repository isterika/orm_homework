<?php

namespace glushkov\orm;

use \glushkov\orm\ArticleTable;
use \glushkov\orm\AuthorTable;

class BlogLinks
{
	/*
	 * Article
	 */
	// create
	static function setArticleAuthor(int $article_id, int $author_id)
	{
		$author = AuthorTable::getByPrimary($author_id)->fetchObject();

		$article = ArticleTable::getByPrimary($article_id)->fetchObject();

		$article->addToAuthor($author);

		$article->save();

		return $article;
	}

	// read
	static function getArticleAuthors(int $id)
	{
		$article = ArticleTable::getByPrimary($id, [
			'select' => ['*', 'AUTHOR']
		])->fetchObject();

		$authors = [];

		foreach ($article->getAuthor() as $author) {
			$authors[] = $author;
		}

		return $authors;
	}

	// delete
	static function removeArticleAuthor(int $article_id, int $author_id)
	{
		$author = AuthorTable::getByPrimary($author_id)->fetchObject();

		$article = ArticleTable::getByPrimary($article_id, [
			'select' => ['*', 'AUTHOR']
		])->fetchObject();

		$article->removeFromAuthor($author);

		$article->save();

		return $article;
	}

	/*
	 * Author
	 */
	// read
	static function getAuthorArticles(int $id)
	{
		$author = AuthorTable::getByPrimary($id, [
			'select' => ['*', 'ARTICLE']
		])->fetchObject();

		$articles = [];

		foreach ($author->getArticle() as $article) {
			$articles[] = $article;
		}

		return $articles;
	}
}
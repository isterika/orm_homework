<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use glushkov\orm\article\AdminInterface\ArticleEditHelper;
use glushkov\orm\article\AdminInterface\ArticleListHelper;
use glushkov\orm\author\AdminInterface\AuthorEditHelper;
use glushkov\orm\author\AdminInterface\AuthorListHelper;

if (
	!Loader::includeModule('digitalwand.admin_helper') ||
	!Loader::includeModule('glushkov.orm')
) {
	return;
}

Loc::loadMessages(__FILE__);

return array(
	array(
		'parent_menu' => 'global_menu_content',
		'sort'        => 1,
		'icon'        => 'fileman_sticker_icon',
		'page_icon'   => 'fileman_sticker_icon',
		'text'        => 'Домашнее задание ORM',
		'url'         => '',
		'items'       => array(
			array(
				'sort'        => 100,
				'icon'        => 'fileman_sticker_icon',
				'page_icon'   => 'fileman_sticker_icon',
				'text'        => 'Статьи',
				'url'         => ArticleListHelper::getUrl(),
				'more_url'    => array(
					ArticleEditHelper::getUrl(),
				)
			),
			array(
				'sort'        => 200,
				'icon'        => 'fileman_sticker_icon',
				'page_icon'   => 'fileman_sticker_icon',
				'text'        => 'Авторы',
				'url'         => AuthorListHelper::getUrl(),
				'more_url'    => array(
					AuthorEditHelper::getUrl(),
				)
			)
		)
	),
);
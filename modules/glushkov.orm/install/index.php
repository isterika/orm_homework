<?php

use Bitrix\Main\Loader;

class glushkov_orm extends CModule
{
	function __construct()
	{
		$arModuleVersion = [];
		include __DIR__ . '/version.php';

		$this->MODULE_ID = 'glushkov.orm';
		$this->MODULE_VERSION = $arModuleVersion['VERSION'];
		$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
		$this->MODULE_NAME = 'ORM';
		$this->MODULE_DESCRIPTION = 'Домашнее задание Bitrix ORM';

		$this->PARTNER_NAME = 'Maxim Glushkov';
		$this->PARTNER_URI = 'https://uplab.ru';
	}

	function DoInstall()
	{
		\Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);

		Loader::includeModule($this->MODULE_ID);

		$this->installTables();
	}

	function DoUninstall()
	{
		Loader::includeModule($this->MODULE_ID);

		$this->unInstallTables();

		\Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);
	}

	function installTables()
	{
	}

	function unInstallTables()
	{
	}
}
<?php

/* ORMENTITYANNOTATION:Bitrix\Socialservices\ApTable */
namespace Bitrix\Socialservices {
	/**
	 * EO_Ap
	 * @see \Bitrix\Socialservices\ApTable
	 *
	 * Custom methods:
	 * ---------------
	 *
	 * @method \int getId()
	 * @method \Bitrix\Socialservices\EO_Ap setId(\int $id)
	 * @method bool hasId()
	 * @method bool isIdFilled()
	 * @method bool isIdChanged()
	 * @method \Bitrix\Main\Type\DateTime getTimestampX()
	 * @method \Bitrix\Socialservices\EO_Ap setTimestampX(\Bitrix\Main\Type\DateTime $timestampX)
	 * @method bool hasTimestampX()
	 * @method bool isTimestampXFilled()
	 * @method bool isTimestampXChanged()
	 * @method \Bitrix\Main\Type\DateTime remindActualTimestampX()
	 * @method \Bitrix\Main\Type\DateTime requireTimestampX()
	 * @method \Bitrix\Socialservices\EO_Ap resetTimestampX()
	 * @method \Bitrix\Socialservices\EO_Ap unsetTimestampX()
	 * @method \Bitrix\Main\Type\DateTime fillTimestampX()
	 * @method \int getUserId()
	 * @method \Bitrix\Socialservices\EO_Ap setUserId(\int $userId)
	 * @method bool hasUserId()
	 * @method bool isUserIdFilled()
	 * @method bool isUserIdChanged()
	 * @method \int remindActualUserId()
	 * @method \int requireUserId()
	 * @method \Bitrix\Socialservices\EO_Ap resetUserId()
	 * @method \Bitrix\Socialservices\EO_Ap unsetUserId()
	 * @method \int fillUserId()
	 * @method \string getDomain()
	 * @method \Bitrix\Socialservices\EO_Ap setDomain(\string $domain)
	 * @method bool hasDomain()
	 * @method bool isDomainFilled()
	 * @method bool isDomainChanged()
	 * @method \string remindActualDomain()
	 * @method \string requireDomain()
	 * @method \Bitrix\Socialservices\EO_Ap resetDomain()
	 * @method \Bitrix\Socialservices\EO_Ap unsetDomain()
	 * @method \string fillDomain()
	 * @method \string getEndpoint()
	 * @method \Bitrix\Socialservices\EO_Ap setEndpoint(\string $endpoint)
	 * @method bool hasEndpoint()
	 * @method bool isEndpointFilled()
	 * @method bool isEndpointChanged()
	 * @method \string remindActualEndpoint()
	 * @method \string requireEndpoint()
	 * @method \Bitrix\Socialservices\EO_Ap resetEndpoint()
	 * @method \Bitrix\Socialservices\EO_Ap unsetEndpoint()
	 * @method \string fillEndpoint()
	 * @method \string getLogin()
	 * @method \Bitrix\Socialservices\EO_Ap setLogin(\string $login)
	 * @method bool hasLogin()
	 * @method bool isLoginFilled()
	 * @method bool isLoginChanged()
	 * @method \string remindActualLogin()
	 * @method \string requireLogin()
	 * @method \Bitrix\Socialservices\EO_Ap resetLogin()
	 * @method \Bitrix\Socialservices\EO_Ap unsetLogin()
	 * @method \string fillLogin()
	 * @method \string getPassword()
	 * @method \Bitrix\Socialservices\EO_Ap setPassword(\string $password)
	 * @method bool hasPassword()
	 * @method bool isPasswordFilled()
	 * @method bool isPasswordChanged()
	 * @method \string remindActualPassword()
	 * @method \string requirePassword()
	 * @method \Bitrix\Socialservices\EO_Ap resetPassword()
	 * @method \Bitrix\Socialservices\EO_Ap unsetPassword()
	 * @method \string fillPassword()
	 * @method \Bitrix\Main\Type\DateTime getLastAuthorize()
	 * @method \Bitrix\Socialservices\EO_Ap setLastAuthorize(\Bitrix\Main\Type\DateTime $lastAuthorize)
	 * @method bool hasLastAuthorize()
	 * @method bool isLastAuthorizeFilled()
	 * @method bool isLastAuthorizeChanged()
	 * @method \Bitrix\Main\Type\DateTime remindActualLastAuthorize()
	 * @method \Bitrix\Main\Type\DateTime requireLastAuthorize()
	 * @method \Bitrix\Socialservices\EO_Ap resetLastAuthorize()
	 * @method \Bitrix\Socialservices\EO_Ap unsetLastAuthorize()
	 * @method \Bitrix\Main\Type\DateTime fillLastAuthorize()
	 * @method \string getSettings()
	 * @method \Bitrix\Socialservices\EO_Ap setSettings(\string $settings)
	 * @method bool hasSettings()
	 * @method bool isSettingsFilled()
	 * @method bool isSettingsChanged()
	 * @method \string remindActualSettings()
	 * @method \string requireSettings()
	 * @method \Bitrix\Socialservices\EO_Ap resetSettings()
	 * @method \Bitrix\Socialservices\EO_Ap unsetSettings()
	 * @method \string fillSettings()
	 *
	 * Common methods:
	 * ---------------
	 *
	 * @property-read \Bitrix\Main\ORM\Entity $entity
	 * @property-read array $primary
	 * @property-read int $state @see \Bitrix\Main\ORM\Objectify\State
	 * @property \Bitrix\Main\Authentication\Context $authContext
	 * @method mixed get($fieldName)
	 * @method mixed remindActual($fieldName)
	 * @method mixed require($fieldName)
	 * @method bool has($fieldName)
	 * @method bool isFilled($fieldName)
	 * @method bool isChanged($fieldName)
	 * @method \Bitrix\Socialservices\EO_Ap set($fieldName, $value)
	 * @method \Bitrix\Socialservices\EO_Ap reset($fieldName)
	 * @method \Bitrix\Socialservices\EO_Ap unset($fieldName)
	 * @method void addTo($fieldName, $value)
	 * @method void removeFrom($fieldName, $value)
	 * @method void removeAll($fieldName)
	 * @method \Bitrix\Main\ORM\Data\Result delete()
	 * @method void fill($fields = \Bitrix\Main\ORM\Fields\FieldTypeMask::ALL) flag or array of field names
	 * @method mixed[] collectValues($valuesType = \Bitrix\Main\ORM\Objectify\Values::ALL, $fieldsMask = \Bitrix\Main\ORM\Fields\FieldTypeMask::ALL)
	 * @method \Bitrix\Main\ORM\Data\AddResult|\Bitrix\Main\ORM\Data\UpdateResult|\Bitrix\Main\ORM\Data\Result save()
	 * @method static \Bitrix\Socialservices\EO_Ap wakeUp($data)
	 */
	class EO_Ap {
		/* @var \Bitrix\Socialservices\ApTable */
		static public $dataClass = '\Bitrix\Socialservices\ApTable';
		public function __construct($setDefaultValues = true) {}
	}
}
namespace Bitrix\Socialservices {
	/**
	 * EO_Ap_Collection
	 *
	 * Custom methods:
	 * ---------------
	 *
	 * @method \int[] getIdList()
	 * @method \Bitrix\Main\Type\DateTime[] getTimestampXList()
	 * @method fillTimestampX()
	 * @method \int[] getUserIdList()
	 * @method fillUserId()
	 * @method \string[] getDomainList()
	 * @method fillDomain()
	 * @method \string[] getEndpointList()
	 * @method fillEndpoint()
	 * @method \string[] getLoginList()
	 * @method fillLogin()
	 * @method \string[] getPasswordList()
	 * @method fillPassword()
	 * @method \Bitrix\Main\Type\DateTime[] getLastAuthorizeList()
	 * @method fillLastAuthorize()
	 * @method \string[] getSettingsList()
	 * @method fillSettings()
	 *
	 * Common methods:
	 * ---------------
	 *
	 * @property-read \Bitrix\Main\ORM\Entity $entity
	 * @method void add(\Bitrix\Socialservices\EO_Ap $object)
	 * @method bool has(\Bitrix\Socialservices\EO_Ap $object)
	 * @method bool hasByPrimary($primary)
	 * @method \Bitrix\Socialservices\EO_Ap getByPrimary($primary)
	 * @method \Bitrix\Socialservices\EO_Ap[] getAll()
	 * @method bool remove(\Bitrix\Socialservices\EO_Ap $object)
	 * @method void removeByPrimary($primary)
	 * @method void fill($fields = \Bitrix\Main\ORM\Fields\FieldTypeMask::ALL) flag or array of field names
	 * @method static \Bitrix\Socialservices\EO_Ap_Collection wakeUp($data)
	 * @method \Bitrix\Main\ORM\Data\Result save($ignoreEvents = false)
	 * @method void offsetSet() ArrayAccess
	 * @method void offsetExists() ArrayAccess
	 * @method void offsetUnset() ArrayAccess
	 * @method void offsetGet() ArrayAccess
	 * @method void rewind() Iterator
	 * @method \Bitrix\Socialservices\EO_Ap current() Iterator
	 * @method mixed key() Iterator
	 * @method void next() Iterator
	 * @method bool valid() Iterator
	 * @method int count() Countable
	 */
	class EO_Ap_Collection implements \ArrayAccess, \Iterator, \Countable {
		/* @var \Bitrix\Socialservices\ApTable */
		static public $dataClass = '\Bitrix\Socialservices\ApTable';
	}
}
namespace Bitrix\Socialservices {
	/**
	 * @method static EO_Ap_Query query()
	 * @method static EO_Ap_Result getByPrimary($primary, array $parameters = array())
	 * @method static EO_Ap_Result getById($id)
	 * @method static EO_Ap_Result getList(array $parameters = array())
	 * @method static EO_Ap_Entity getEntity()
	 * @method static \Bitrix\Socialservices\EO_Ap createObject($setDefaultValues = true)
	 * @method static \Bitrix\Socialservices\EO_Ap_Collection createCollection()
	 * @method static \Bitrix\Socialservices\EO_Ap wakeUpObject($row)
	 * @method static \Bitrix\Socialservices\EO_Ap_Collection wakeUpCollection($rows)
	 */
	class ApTable extends \Bitrix\Main\ORM\Data\DataManager {}
	/**
	 * @method EO_Ap_Result exec()
	 * @method \Bitrix\Socialservices\EO_Ap fetchObject()
	 * @method \Bitrix\Socialservices\EO_Ap_Collection fetchCollection()
	 */
	class EO_Ap_Query extends \Bitrix\Main\ORM\Query\Query {}
	/**
	 * @method \Bitrix\Socialservices\EO_Ap fetchObject()
	 * @method \Bitrix\Socialservices\EO_Ap_Collection fetchCollection()
	 */
	class EO_Ap_Result extends \Bitrix\Main\ORM\Query\Result {}
	/**
	 * @method \Bitrix\Socialservices\EO_Ap createObject($setDefaultValues = true)
	 * @method \Bitrix\Socialservices\EO_Ap_Collection createCollection()
	 * @method \Bitrix\Socialservices\EO_Ap wakeUpObject($row)
	 * @method \Bitrix\Socialservices\EO_Ap_Collection wakeUpCollection($rows)
	 */
	class EO_Ap_Entity extends \Bitrix\Main\ORM\Entity {}
}
/* ORMENTITYANNOTATION:glushkov\orm\ArticleTable */
namespace glushkov\orm {
	/**
	 * EO_Article
	 * @see \glushkov\orm\ArticleTable
	 *
	 * Custom methods:
	 * ---------------
	 *
	 * @method \int getId()
	 * @method \glushkov\orm\EO_Article setId(\int $id)
	 * @method bool hasId()
	 * @method bool isIdFilled()
	 * @method bool isIdChanged()
	 * @method \string getTitle()
	 * @method \glushkov\orm\EO_Article setTitle(\string $title)
	 * @method bool hasTitle()
	 * @method bool isTitleFilled()
	 * @method bool isTitleChanged()
	 * @method \string remindActualTitle()
	 * @method \string requireTitle()
	 * @method \glushkov\orm\EO_Article resetTitle()
	 * @method \glushkov\orm\EO_Article unsetTitle()
	 * @method \string fillTitle()
	 * @method \string getPreviewText()
	 * @method \glushkov\orm\EO_Article setPreviewText(\string $previewText)
	 * @method bool hasPreviewText()
	 * @method bool isPreviewTextFilled()
	 * @method bool isPreviewTextChanged()
	 * @method \string remindActualPreviewText()
	 * @method \string requirePreviewText()
	 * @method \glushkov\orm\EO_Article resetPreviewText()
	 * @method \glushkov\orm\EO_Article unsetPreviewText()
	 * @method \string fillPreviewText()
	 * @method \string getDetailText()
	 * @method \glushkov\orm\EO_Article setDetailText(\string $detailText)
	 * @method bool hasDetailText()
	 * @method bool isDetailTextFilled()
	 * @method bool isDetailTextChanged()
	 * @method \string remindActualDetailText()
	 * @method \string requireDetailText()
	 * @method \glushkov\orm\EO_Article resetDetailText()
	 * @method \glushkov\orm\EO_Article unsetDetailText()
	 * @method \string fillDetailText()
	 * @method \Bitrix\Main\Type\Date getCreated()
	 * @method \glushkov\orm\EO_Article setCreated(\Bitrix\Main\Type\Date $created)
	 * @method bool hasCreated()
	 * @method bool isCreatedFilled()
	 * @method bool isCreatedChanged()
	 * @method \Bitrix\Main\Type\Date remindActualCreated()
	 * @method \Bitrix\Main\Type\Date requireCreated()
	 * @method \glushkov\orm\EO_Article resetCreated()
	 * @method \glushkov\orm\EO_Article unsetCreated()
	 * @method \Bitrix\Main\Type\Date fillCreated()
	 * @method \Bitrix\Main\Type\Date getModified()
	 * @method \glushkov\orm\EO_Article setModified(\Bitrix\Main\Type\Date $modified)
	 * @method bool hasModified()
	 * @method bool isModifiedFilled()
	 * @method bool isModifiedChanged()
	 * @method \Bitrix\Main\Type\Date remindActualModified()
	 * @method \Bitrix\Main\Type\Date requireModified()
	 * @method \glushkov\orm\EO_Article resetModified()
	 * @method \glushkov\orm\EO_Article unsetModified()
	 * @method \Bitrix\Main\Type\Date fillModified()
	 * @method \glushkov\orm\EO_Author_Collection getAuthor()
	 * @method \glushkov\orm\EO_Author_Collection requireAuthor()
	 * @method \glushkov\orm\EO_Author_Collection fillAuthor()
	 * @method bool hasAuthor()
	 * @method bool isAuthorFilled()
	 * @method bool isAuthorChanged()
	 * @method void addToAuthor(\glushkov\orm\EO_Author $author)
	 * @method void removeFromAuthor(\glushkov\orm\EO_Author $author)
	 * @method void removeAllAuthor()
	 * @method \glushkov\orm\EO_Article resetAuthor()
	 * @method \glushkov\orm\EO_Article unsetAuthor()
	 *
	 * Common methods:
	 * ---------------
	 *
	 * @property-read \Bitrix\Main\ORM\Entity $entity
	 * @property-read array $primary
	 * @property-read int $state @see \Bitrix\Main\ORM\Objectify\State
	 * @property \Bitrix\Main\Authentication\Context $authContext
	 * @method mixed get($fieldName)
	 * @method mixed remindActual($fieldName)
	 * @method mixed require($fieldName)
	 * @method bool has($fieldName)
	 * @method bool isFilled($fieldName)
	 * @method bool isChanged($fieldName)
	 * @method \glushkov\orm\EO_Article set($fieldName, $value)
	 * @method \glushkov\orm\EO_Article reset($fieldName)
	 * @method \glushkov\orm\EO_Article unset($fieldName)
	 * @method void addTo($fieldName, $value)
	 * @method void removeFrom($fieldName, $value)
	 * @method void removeAll($fieldName)
	 * @method \Bitrix\Main\ORM\Data\Result delete()
	 * @method void fill($fields = \Bitrix\Main\ORM\Fields\FieldTypeMask::ALL) flag or array of field names
	 * @method mixed[] collectValues($valuesType = \Bitrix\Main\ORM\Objectify\Values::ALL, $fieldsMask = \Bitrix\Main\ORM\Fields\FieldTypeMask::ALL)
	 * @method \Bitrix\Main\ORM\Data\AddResult|\Bitrix\Main\ORM\Data\UpdateResult|\Bitrix\Main\ORM\Data\Result save()
	 * @method static \glushkov\orm\EO_Article wakeUp($data)
	 */
	class EO_Article {
		/* @var \glushkov\orm\ArticleTable */
		static public $dataClass = '\glushkov\orm\ArticleTable';
		public function __construct($setDefaultValues = true) {}
	}
}
namespace glushkov\orm {
	/**
	 * EO_Article_Collection
	 *
	 * Custom methods:
	 * ---------------
	 *
	 * @method \int[] getIdList()
	 * @method \string[] getTitleList()
	 * @method fillTitle()
	 * @method \string[] getPreviewTextList()
	 * @method fillPreviewText()
	 * @method \string[] getDetailTextList()
	 * @method fillDetailText()
	 * @method \Bitrix\Main\Type\Date[] getCreatedList()
	 * @method fillCreated()
	 * @method \Bitrix\Main\Type\Date[] getModifiedList()
	 * @method fillModified()
	 * @method \glushkov\orm\EO_Author_Collection[] getAuthorList()
	 * @method fillAuthor()
	 *
	 * Common methods:
	 * ---------------
	 *
	 * @property-read \Bitrix\Main\ORM\Entity $entity
	 * @method void add(\glushkov\orm\EO_Article $object)
	 * @method bool has(\glushkov\orm\EO_Article $object)
	 * @method bool hasByPrimary($primary)
	 * @method \glushkov\orm\EO_Article getByPrimary($primary)
	 * @method \glushkov\orm\EO_Article[] getAll()
	 * @method bool remove(\glushkov\orm\EO_Article $object)
	 * @method void removeByPrimary($primary)
	 * @method void fill($fields = \Bitrix\Main\ORM\Fields\FieldTypeMask::ALL) flag or array of field names
	 * @method static \glushkov\orm\EO_Article_Collection wakeUp($data)
	 * @method \Bitrix\Main\ORM\Data\Result save($ignoreEvents = false)
	 * @method void offsetSet() ArrayAccess
	 * @method void offsetExists() ArrayAccess
	 * @method void offsetUnset() ArrayAccess
	 * @method void offsetGet() ArrayAccess
	 * @method void rewind() Iterator
	 * @method \glushkov\orm\EO_Article current() Iterator
	 * @method mixed key() Iterator
	 * @method void next() Iterator
	 * @method bool valid() Iterator
	 * @method int count() Countable
	 */
	class EO_Article_Collection implements \ArrayAccess, \Iterator, \Countable {
		/* @var \glushkov\orm\ArticleTable */
		static public $dataClass = '\glushkov\orm\ArticleTable';
	}
}
namespace glushkov\orm {
	/**
	 * @method static EO_Article_Query query()
	 * @method static EO_Article_Result getByPrimary($primary, array $parameters = array())
	 * @method static EO_Article_Result getById($id)
	 * @method static EO_Article_Result getList(array $parameters = array())
	 * @method static EO_Article_Entity getEntity()
	 * @method static \glushkov\orm\EO_Article createObject($setDefaultValues = true)
	 * @method static \glushkov\orm\EO_Article_Collection createCollection()
	 * @method static \glushkov\orm\EO_Article wakeUpObject($row)
	 * @method static \glushkov\orm\EO_Article_Collection wakeUpCollection($rows)
	 */
	class ArticleTable extends \Bitrix\Main\ORM\Data\DataManager {}
	/**
	 * @method EO_Article_Result exec()
	 * @method \glushkov\orm\EO_Article fetchObject()
	 * @method \glushkov\orm\EO_Article_Collection fetchCollection()
	 */
	class EO_Article_Query extends \Bitrix\Main\ORM\Query\Query {}
	/**
	 * @method \glushkov\orm\EO_Article fetchObject()
	 * @method \glushkov\orm\EO_Article_Collection fetchCollection()
	 */
	class EO_Article_Result extends \Bitrix\Main\ORM\Query\Result {}
	/**
	 * @method \glushkov\orm\EO_Article createObject($setDefaultValues = true)
	 * @method \glushkov\orm\EO_Article_Collection createCollection()
	 * @method \glushkov\orm\EO_Article wakeUpObject($row)
	 * @method \glushkov\orm\EO_Article_Collection wakeUpCollection($rows)
	 */
	class EO_Article_Entity extends \Bitrix\Main\ORM\Entity {}
}
/* ORMENTITYANNOTATION:glushkov\orm\AuthorTable */
namespace glushkov\orm {
	/**
	 * EO_Author
	 * @see \glushkov\orm\AuthorTable
	 *
	 * Custom methods:
	 * ---------------
	 *
	 * @method \int getId()
	 * @method \glushkov\orm\EO_Author setId(\int $id)
	 * @method bool hasId()
	 * @method bool isIdFilled()
	 * @method bool isIdChanged()
	 * @method \string getName()
	 * @method \glushkov\orm\EO_Author setName(\string $name)
	 * @method bool hasName()
	 * @method bool isNameFilled()
	 * @method bool isNameChanged()
	 * @method \string remindActualName()
	 * @method \string requireName()
	 * @method \glushkov\orm\EO_Author resetName()
	 * @method \glushkov\orm\EO_Author unsetName()
	 * @method \string fillName()
	 * @method \Bitrix\Main\Type\Date getDateOfBirth()
	 * @method \glushkov\orm\EO_Author setDateOfBirth(\Bitrix\Main\Type\Date $dateOfBirth)
	 * @method bool hasDateOfBirth()
	 * @method bool isDateOfBirthFilled()
	 * @method bool isDateOfBirthChanged()
	 * @method \Bitrix\Main\Type\Date remindActualDateOfBirth()
	 * @method \Bitrix\Main\Type\Date requireDateOfBirth()
	 * @method \glushkov\orm\EO_Author resetDateOfBirth()
	 * @method \glushkov\orm\EO_Author unsetDateOfBirth()
	 * @method \Bitrix\Main\Type\Date fillDateOfBirth()
	 * @method \Bitrix\Main\Type\Date getCreated()
	 * @method \glushkov\orm\EO_Author setCreated(\Bitrix\Main\Type\Date $created)
	 * @method bool hasCreated()
	 * @method bool isCreatedFilled()
	 * @method bool isCreatedChanged()
	 * @method \Bitrix\Main\Type\Date remindActualCreated()
	 * @method \Bitrix\Main\Type\Date requireCreated()
	 * @method \glushkov\orm\EO_Author resetCreated()
	 * @method \glushkov\orm\EO_Author unsetCreated()
	 * @method \Bitrix\Main\Type\Date fillCreated()
	 * @method \Bitrix\Main\Type\Date getModified()
	 * @method \glushkov\orm\EO_Author setModified(\Bitrix\Main\Type\Date $modified)
	 * @method bool hasModified()
	 * @method bool isModifiedFilled()
	 * @method bool isModifiedChanged()
	 * @method \Bitrix\Main\Type\Date remindActualModified()
	 * @method \Bitrix\Main\Type\Date requireModified()
	 * @method \glushkov\orm\EO_Author resetModified()
	 * @method \glushkov\orm\EO_Author unsetModified()
	 * @method \Bitrix\Main\Type\Date fillModified()
	 * @method \glushkov\orm\EO_Article_Collection getArticle()
	 * @method \glushkov\orm\EO_Article_Collection requireArticle()
	 * @method \glushkov\orm\EO_Article_Collection fillArticle()
	 * @method bool hasArticle()
	 * @method bool isArticleFilled()
	 * @method bool isArticleChanged()
	 * @method void addToArticle(\glushkov\orm\EO_Article $article)
	 * @method void removeFromArticle(\glushkov\orm\EO_Article $article)
	 * @method void removeAllArticle()
	 * @method \glushkov\orm\EO_Author resetArticle()
	 * @method \glushkov\orm\EO_Author unsetArticle()
	 *
	 * Common methods:
	 * ---------------
	 *
	 * @property-read \Bitrix\Main\ORM\Entity $entity
	 * @property-read array $primary
	 * @property-read int $state @see \Bitrix\Main\ORM\Objectify\State
	 * @property \Bitrix\Main\Authentication\Context $authContext
	 * @method mixed get($fieldName)
	 * @method mixed remindActual($fieldName)
	 * @method mixed require($fieldName)
	 * @method bool has($fieldName)
	 * @method bool isFilled($fieldName)
	 * @method bool isChanged($fieldName)
	 * @method \glushkov\orm\EO_Author set($fieldName, $value)
	 * @method \glushkov\orm\EO_Author reset($fieldName)
	 * @method \glushkov\orm\EO_Author unset($fieldName)
	 * @method void addTo($fieldName, $value)
	 * @method void removeFrom($fieldName, $value)
	 * @method void removeAll($fieldName)
	 * @method \Bitrix\Main\ORM\Data\Result delete()
	 * @method void fill($fields = \Bitrix\Main\ORM\Fields\FieldTypeMask::ALL) flag or array of field names
	 * @method mixed[] collectValues($valuesType = \Bitrix\Main\ORM\Objectify\Values::ALL, $fieldsMask = \Bitrix\Main\ORM\Fields\FieldTypeMask::ALL)
	 * @method \Bitrix\Main\ORM\Data\AddResult|\Bitrix\Main\ORM\Data\UpdateResult|\Bitrix\Main\ORM\Data\Result save()
	 * @method static \glushkov\orm\EO_Author wakeUp($data)
	 */
	class EO_Author {
		/* @var \glushkov\orm\AuthorTable */
		static public $dataClass = '\glushkov\orm\AuthorTable';
		public function __construct($setDefaultValues = true) {}
	}
}
namespace glushkov\orm {
	/**
	 * EO_Author_Collection
	 *
	 * Custom methods:
	 * ---------------
	 *
	 * @method \int[] getIdList()
	 * @method \string[] getNameList()
	 * @method fillName()
	 * @method \Bitrix\Main\Type\Date[] getDateOfBirthList()
	 * @method fillDateOfBirth()
	 * @method \Bitrix\Main\Type\Date[] getCreatedList()
	 * @method fillCreated()
	 * @method \Bitrix\Main\Type\Date[] getModifiedList()
	 * @method fillModified()
	 * @method \glushkov\orm\EO_Article_Collection[] getArticleList()
	 * @method fillArticle()
	 *
	 * Common methods:
	 * ---------------
	 *
	 * @property-read \Bitrix\Main\ORM\Entity $entity
	 * @method void add(\glushkov\orm\EO_Author $object)
	 * @method bool has(\glushkov\orm\EO_Author $object)
	 * @method bool hasByPrimary($primary)
	 * @method \glushkov\orm\EO_Author getByPrimary($primary)
	 * @method \glushkov\orm\EO_Author[] getAll()
	 * @method bool remove(\glushkov\orm\EO_Author $object)
	 * @method void removeByPrimary($primary)
	 * @method void fill($fields = \Bitrix\Main\ORM\Fields\FieldTypeMask::ALL) flag or array of field names
	 * @method static \glushkov\orm\EO_Author_Collection wakeUp($data)
	 * @method \Bitrix\Main\ORM\Data\Result save($ignoreEvents = false)
	 * @method void offsetSet() ArrayAccess
	 * @method void offsetExists() ArrayAccess
	 * @method void offsetUnset() ArrayAccess
	 * @method void offsetGet() ArrayAccess
	 * @method void rewind() Iterator
	 * @method \glushkov\orm\EO_Author current() Iterator
	 * @method mixed key() Iterator
	 * @method void next() Iterator
	 * @method bool valid() Iterator
	 * @method int count() Countable
	 */
	class EO_Author_Collection implements \ArrayAccess, \Iterator, \Countable {
		/* @var \glushkov\orm\AuthorTable */
		static public $dataClass = '\glushkov\orm\AuthorTable';
	}
}
namespace glushkov\orm {
	/**
	 * @method static EO_Author_Query query()
	 * @method static EO_Author_Result getByPrimary($primary, array $parameters = array())
	 * @method static EO_Author_Result getById($id)
	 * @method static EO_Author_Result getList(array $parameters = array())
	 * @method static EO_Author_Entity getEntity()
	 * @method static \glushkov\orm\EO_Author createObject($setDefaultValues = true)
	 * @method static \glushkov\orm\EO_Author_Collection createCollection()
	 * @method static \glushkov\orm\EO_Author wakeUpObject($row)
	 * @method static \glushkov\orm\EO_Author_Collection wakeUpCollection($rows)
	 */
	class AuthorTable extends \Bitrix\Main\ORM\Data\DataManager {}
	/**
	 * @method EO_Author_Result exec()
	 * @method \glushkov\orm\EO_Author fetchObject()
	 * @method \glushkov\orm\EO_Author_Collection fetchCollection()
	 */
	class EO_Author_Query extends \Bitrix\Main\ORM\Query\Query {}
	/**
	 * @method \glushkov\orm\EO_Author fetchObject()
	 * @method \glushkov\orm\EO_Author_Collection fetchCollection()
	 */
	class EO_Author_Result extends \Bitrix\Main\ORM\Query\Result {}
	/**
	 * @method \glushkov\orm\EO_Author createObject($setDefaultValues = true)
	 * @method \glushkov\orm\EO_Author_Collection createCollection()
	 * @method \glushkov\orm\EO_Author wakeUpObject($row)
	 * @method \glushkov\orm\EO_Author_Collection wakeUpCollection($rows)
	 */
	class EO_Author_Entity extends \Bitrix\Main\ORM\Entity {}
}